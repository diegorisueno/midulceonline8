const express = require('express')
const app = express()

const cors = require("cors");
//Habilitar cors
app.use(cors())

//Importo la conexion con mongoDB
const miconexion = require('./conexion')
//Importo el body parser
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))



const rutas = require('./routers/routers')
app.use('/api', rutas)


const PORT = process.env.PORT || 4500;

//Peticion get por defecto
app.get('/', (req, res) => {
    res.end("Servidor Backend OK!")
})

//Servidor 
app.listen(PORT, () => {
    console.log(`El servidor esta Funcionando en el ${PORT}`);
});