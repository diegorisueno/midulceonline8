const mongoose = require('mongoose')
const miesquema = mongoose.Schema
const esquemaFactura = new miesquema({
    //_id : miesquema.Types.ObjectId, 
    idFactura : String,
    idPedido : String,
    idProducto : String,
    totalFactura : Number,
    activo : Boolean
});
const modeloFactura = mongoose.model('facturas', esquemaFactura)
module.exports = modeloFactura