const mongoose = require('mongoose')
const miesquema = mongoose.Schema
const esquemaPedido = new miesquema({
    //_id : miesquema.Types.ObjectId, 
    idPedido : String,
    idProducto : String,
    idCliente : String,
    cantidad : Number,
    activo : Boolean
});
const modeloPedido = mongoose.model('pedidos', esquemaPedido)
module.exports = modeloPedido