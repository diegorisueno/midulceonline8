const mongoose = require('mongoose')
const miesquema = mongoose.Schema
const esquemaProducto = new miesquema({
    //_id : miesquema.Types.ObjectId, 
    idProducto: String,
    nombreProducto: String,
    unidades: Number,
    precio: Number,
    activo: Boolean
});
const modeloProducto = mongoose.model('productos', esquemaProducto)
module.exports = modeloProducto
