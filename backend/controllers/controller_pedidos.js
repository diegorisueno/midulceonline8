const express = require('express');
const router = express.Router();
const modeloPedido = require('../models/model_pedidos');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un producto http://localhost:5000/api/productos/cargar/
router.get('/listar', (req, res) => {
    modeloPedido.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevoPedido = new modeloPedido({
        idPedido: req.body.idPedido,
        idProducto: req.body.idProducto,
        idCliente: req.body.idCliente,
        cantidad: req.body.cantidad,
        activo: req.body.activo
    })
    //nuevoProducto._id = new mongoose.Types.ObjectId();
    //nuevoProducto.set('versionKey', false);
    nuevoPedido.save(function(err)
    {
        if(!err)
        {
            res.send('El pedido fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un producto http://localhost:5000/api/productos/cargar/1
router.get('/cargar/:idPedido', (req, res) => {
    modeloPedido.find({idPedido:req.params.idPedido}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/editar/2
router.post('/editar/:idPedido', (req, res) => {
    modeloPedido.findOneAndUpdate(
        {idPedido:req.params.idPedido}
        ,{
            idProducto: req.body.idProducto,
            idCliente: req.body.idCliente,
            cantidad: req.body.cantidad,
            activo: req.body.activo
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El Pedido se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/productos/borrar/3
router.delete('/borrar/:idPedido', (req, res) => {
    modeloPedido.findOneAndDelete(
        {idPedido:req.params.idPedido},
        (err) =>
        {
            if(!err)
            {
                res.send("El Pedido se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router