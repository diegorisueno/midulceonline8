const modeloProductos = require('../models/model_productos');

exports.productosAgregar = async (req, res) => {
  const nuevoProducto = new modeloProductos({
    idProducto: req.body.idProducto,
    nombreProducto: req.body.nombreProducto,
    unidades: req.body.unidades,
    precio: req.body.precio,
    activo: req.body.activo
  })
  nuevoProducto.save()
    res.json({msg: "El producto fue guardado exitosamente"});
}

exports.productosListar = async (req, res) => {
  try {
    const productos = await modeloProductos.find();
    res.json({productos})
  } catch (error) {
        res.status(400).send("Hubo un error");
  }
}


exports.productosCargar = async (req, res) => {
  try {
    const productos = await modeloProductos.find({idProducto: req.params.idProducto});
  res.json({productos})
  } catch (error) {
    res.status(400).send("Hubo un error");
  }
}

exports.productosEditar = async (req, res) => {
await modeloProductos.findOneAndUpdate(
      {idProducto:req.params.idProducto}
      ,{
        idProducto: req.body.idProducto,
        nombreProducto: req.body.nombreProducto,
        unidades: req.body.unidades,
        precio: req.body.precio,
        activo: req.body.activo
      })
      res.json({msg: "actualizar el registro"})
}

exports.productosBorrar = async (req, res) => {
    await modeloProductos.findOneAndDelete(
      {idProducto:req.params.idProducto})
      res.json({msg: "producto eliminado "})
}