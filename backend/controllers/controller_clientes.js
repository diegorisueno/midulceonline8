const express = require('express');
const router = express.Router();
const modeloCliente = require('../models/model_clientes');

//METODO <<SUGERIDO>> POR EL PROFESOR
//Cargar un producto http://localhost:5000/api/productos/cargar/
router.get('/listar', (req, res) => {
    modeloCliente.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/agregar/
router.post('/agregar', (req, res) => {
    
    const nuevoCliente = new modeloCliente({
        idCliente: req.body.idCliente,
        nombreCliente: req.body.nombreCliente,
        emailCliente: req.body.emailCliente,
        contraseñaCliente: req.body.contraseñaCliente,
        activo: req.body.activo
    })
    //nuevoProducto._id = new mongoose.Types.ObjectId();
    //nuevoProducto.set('versionKey', false);
    nuevoCliente.save(function(err)
    {
        if(!err)
        {
            res.send('El cliente fue agregado exitosamente!!!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//Cargar un producto http://localhost:5000/api/productos/cargar/1
router.get('/cargar/:idCliente', (req, res) => {
    modeloCliente.find({idCliente:req.params.idCliente}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//http://localhost:5000/api/productos/editar/2
router.post('/editar/:idCliente', (req, res) => {
    modeloCliente.findOneAndUpdate(
        {idCliente:req.params.idCliente}
        ,{
            nombreCliente: req.body.nombreCliente,
            emailCliente: req.body.emailCliente,
            contraseñaCliente: req.body.contraseñaCliente,
            activo: req.body.activo,
        },
        (err) =>
        {
            if(!err)
            {
                res.send("El Cliente se actualizó exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

//http://localhost:5000/api/productos/borrar/3
router.delete('/borrar/:idCliente', (req, res) => {
    modeloCliente.findOneAndDelete(
        {idCliente:req.params.idCliente},
        (err) =>
        {
            if(!err)
            {
                res.send("El Cliente se elimino exitosamente!!!")
            }
            else
            {
                res.send(err)
            }
        })
})

module.exports = router