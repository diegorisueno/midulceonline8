const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const ModelImagen = require('../models/model_imagen');

const app = express(); //Expres en la raiz

app.use(bodyParser.json())
app.use(fileUpload({
    limits: {fileSize: 50 * 1024 * 1024},
}));

exports.imagenesAgregar = async (req, res) => {
    let variable = req.params.id;
    let body = req.body.producto;
    let imagen = req.files.imagen;

    let data = {
        producto_nombre: req.body.nombre
    }

    let modelImagen = new ModelImagen(data);

    console.log(req.files.imagen);

    modelImagen.imagen.data = req.files.imagen.data;
    modelImagen.imagen.contentType = req.files.imagen.mimetype;

    modelImagen.save( (err,rpta) => {

        if (err){
            res.json({
            err:err,
            })
        }

        res.json({
            result:true,
        })

    })
}

exports.imagenesCargar = async (req, res) => {
    ModelImagen.findById(req.params.id).exec((err,doc) => {
        if(err){
            res.json({
                err: err,
            })
        }

        console.log('imagen',doc);

        res.set('Content-Type', doc.imagen.contentType);
        return res.send(doc.imagen.data)
    })
}
