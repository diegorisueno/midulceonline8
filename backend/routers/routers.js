const express = require('express');
const router = express.Router();

const routerAuth = require("./auth");
const routerProductos = require('./router_productos');
const controladorProductosSinAuth = require('./router_productos');
const rutaUsuarios = require('./router_usuarios');
const rutaImagenes = require('./router_imagenes');



router.use("/auth", routerAuth);
router.use("/usuarios",rutaUsuarios);
router.use("/productos",routerProductos);
router.use("/productossinauth",controladorProductosSinAuth);
router.use("/imagenes",rutaImagenes);


module.exports = router



