const express = require('express');
const router = express.Router();

const auth = require("../middleware/auth");

//metodos utilizados
const modelo = require('../controllers/controller_imagenes')
router.post("/guardarimg", modelo.imagenesAgregar)
router.get("/cargarimg/:id", modelo.imagenesCargar)
//router.put("/updateimg/:id", modelo.productosEditar)

module.exports = router