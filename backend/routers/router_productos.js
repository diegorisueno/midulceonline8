const express = require('express');
const router = express.Router();

const auth = require("../middleware/auth");

//metodos utilizados
const modelo = require('../controllers/controller_productos_leonardo')
router.get("/listardos", modelo.productosListar)
router.post("/save", modelo.productosAgregar)
router.delete("/delete/:idProducto", modelo.productosBorrar)
router.get("/cargar/:idProducto", modelo.productosCargar)
router.put("/update/:idProducto", modelo.productosEditar)

module.exports = router
