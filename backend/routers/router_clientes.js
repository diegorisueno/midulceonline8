const express = require('express');
const router = express.Router();

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorClientes = require('../controllers/controller_clientes');
router.get("/listar",controladorClientes);
router.get("/cargar/:idCliente",controladorClientes);
router.post("/agregar",controladorClientes);
router.post("/editar/:idCliente",controladorClientes);
router.delete("/borrar/:idCliente",controladorClientes);

module.exports = router