const express = require('express');
const router = express.Router();

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorPedidos = require('../controllers/controller_pedidos');
router.get("/listar",controladorPedidos);
router.get("/cargar/:idPedido",controladorPedidos);
router.post("/agregar",controladorPedidos);
router.post("/editar/:idPedido",controladorPedidos);
router.delete("/borrar/:idPedido",controladorPedidos);

module.exports = router