const express = require('express');
const router = express.Router();

//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
const controladorFacturas = require('../controllers/controller_facturas');
router.get("/listar",controladorFacturas);
router.get("/cargar/:idFactura",controladorFacturas);
router.post("/agregar",controladorFacturas);
router.post("/editar/:idFactura",controladorFacturas);
router.delete("/borrar/:idFactura",controladorFacturas);

module.exports = router