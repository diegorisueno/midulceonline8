//import logo from './logo.svg';
//import './App.css';
import React, {Fragment} from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

import Login from './auth/Login';

import PaginaPrincipal from './componentes/paginaprincipal';
import CrearCuenta from './auth/CrearCuenta';
import ProductosAgregar from './componentes/productosagregar';
import ProductosEditar from './componentes/productoseditar';
import Home from './componentes/home';

function App()
{
  return(
  <main className='App'>
    <Fragment>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Login/>} exact/>
          <Route path="/crearcuenta" element={<CrearCuenta />} exact />
          <Route path='/home' element={<Home />} exact/>
          <Route path='/inicio' element={<PaginaPrincipal/>} exact></Route>
          <Route path='/productosAgregar' element={<ProductosAgregar/>} exact></Route>
          <Route path='/productosEditar/:idProducto' element={<ProductosEditar/>} exact></Route>
        </Routes>
      </BrowserRouter>
    </Fragment>
  </main>
  )
}
export default App;
