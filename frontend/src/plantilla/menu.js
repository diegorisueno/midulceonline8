import React from 'react';

import {Link} from 'react-router-dom';

function Menu()
{

    return(
        <div>
            <div className="loader-bg">
            <div className="loader-track">
                <div className="loader-fill"></div>
            </div>
            </div>
            <nav className="pcoded-navbar">
            <div className="navbar-wrapper">
                <div className="navbar-content scroll-div">
                    <div className="">
                        <div className="main-menu-header">
                            <img className="img-radius" src="assets/images/user/LogoMidulceonline.png" alt=""></img>
                            <div className="user-details">
                                <span>MiDulceOnline</span>
                                <div id="more-details">Grupo 8<i className="fa fa-chevron-down m-l-5"></i></div>
                            </div>
                        </div>
                        <div className="collapse" id="nav-user-link">
                            <ul className="list-unstyled">
                                <li className="list-group-item"><a href="user-profile.html"><i className="feather icon-user m-r-5"></i>Perfil</a></li>
                                <li className="list-group-item"><a href="#!"><i className="feather icon-settings m-r-5"></i>Configuración</a></li>
                                <li className="list-group-item"><a href="auth-normal-sign-in.html"><i className="feather icon-log-out m-r-5"></i>Cerrar sesión</a></li>
                            </ul>
                        </div>
                    </div>
                    <ul className="nav pcoded-inner-navbar ">
                        <li className="nav-item pcoded-menu-caption">
                            <label>Mi tienda virtual</label>
                        </li>
                        <li className="nav-item">
                            <Link to={"/home"} className="nav-link "><span className="pcoded-micon"><i className="feather icon-home"></i></span><span className="pcoded-mtext">Inicio</span></Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/inicio"} className="nav-link "><span className="pcoded-micon"><i className="feather icon-layout"></i></span><span className="pcoded-mtext">Productos</span></Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/"} className="nav-link "><span className="pcoded-micon"><i className="feather icon-user"></i></span><span className="pcoded-mtext">Inicio/Cierre sesión</span></Link>
                        </li>
                    </ul>
                </div>
            </div>
            </nav>
        </div>
    )
}

export default Menu
