
import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
//import ProductosBorrar from './productosborrar';
import Swal from 'sweetalert2';

import APIInvoke from "../utils/APIInvoke";


function ProductosListar () {
 
    const [dataProductos, setdataProductos] = useState([]);
    
    const listaProductos = async () => {
      const response = await APIInvoke.invokeGET(`/api/productos/listardos`);
      setdataProductos(response.productos);
    };


    function ProductosBorrar(e, idProducto)
    {    
    e.preventDefault();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({title: '¿Realmente desea eliminar este registro?',
        text: "No es posible revertir este cambio",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
        }).then((result) => {
        if (result.isConfirmed) {
        
        APIInvoke.invokeDELETE(`/api/productos/delete/${idProducto}`);
            //consulta los datos
            listaProductos();

        swalWithBootstrapButtons.fire(
            '¡Operación Exitosa!',
            'El registro ha sido eliminado exitosamente',
            'success'
        )
        } else if (
        result.dismiss === Swal.DismissReason.cancel
        ) {
        swalWithBootstrapButtons.fire(
            '¡ERROR!',
            'El registro no pudo ser eliminado',
            'error'
            )
        }
        })
    
        }

      //Refrescar la pagina 
    useEffect(() => {
    listaProductos();
    }, []);

    return(
        <section className="pcoded-main-container">
            <div className="pcoded-content">
                <div className="page-header">
                    <div className="page-block">
                        <div className="row align-items-center">
                            <div className="col-md-12">
                                <div className="page-header-title">
                                    <h5 className="m-b-10">MiDulceOnline8 tienda virtual</h5>
                                </div>
                                <ul className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="/"><i className="feather icon-home"></i></Link></li>
                                    <li className="breadcrumb-item"><Link to="/inicio">Inventario</Link></li>
                                    <li className="breadcrumb-item"><Link to="/inicio">Productos</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                <div className="col-md-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>Agregar de Productos</h5>
                            <span className="d-block m-t-5"></span>
                            <div>
                            <td align="center"><Link to={"/productosAgregar"}><button type="button" className="btn btn-icon btn-primary"><i className="feather icon-plus-circle"></i></button></Link></td>
                            </div>
                        </div>
                        <div className="card-body table-border-style">
                            <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td align="center">Id</td>
                                            <td align="center">Nombre</td>
                                            <td align="center">Unidades</td>
                                            <td align="center">Precio</td>
                                            <td align="center">Activo</td>
                                            <td align="center">Editar</td>
                                            <td align="center">Eliminar</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {dataProductos.map(miproducto => (
                                        <tr key={miproducto._id}>
                                            <td align="center">{miproducto.idProducto}</td>
                                            <td align="center">{miproducto.nombreProducto}</td>
                                            <td align="center">{miproducto.unidades}</td>
                                            <td align="center">{miproducto.precio}</td>
                                            <td align="center">{miproducto.activo ? 'Activo' : 'Inactivo'}</td>
                                            <td align="center"><Link to={`/productosEditar/${miproducto.idProducto}`}><button type="button" className="btn  btn-icon btn-primary"><i className="feather icon-edit"></i></button></Link></td>
                                            <td align="center"><button type="button" className="btn btn-icon btn-danger" onClick={(e) => ProductosBorrar(e, miproducto.idProducto)}><i className="feather icon-trash"></i></button></td>                                           
                                        </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>  
    )
}

export default ProductosListar;

