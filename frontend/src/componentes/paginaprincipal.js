import React from 'react';
import Menu from '../plantilla/menu';
import Logo from '../plantilla/logo';
import ProductosListar from './productoslistar';

function PaginaPrincipal()
{
    return(
        <div className="">
            <Menu/>
            <Logo/>
            <ProductosListar/>
            
        </div>
    )
}
export default PaginaPrincipal;