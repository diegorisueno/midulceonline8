import React from "react";
import Logo from "../plantilla/logo";
import Menu from "../plantilla/menu";

function home() {
  return (
    <div>
      <Logo />
      <Menu />
      <section className="pcoded-main-container">
        <div className="pcoded-content">
          <div className="page-header">
            <div className="page-block">
              <div className="row align-items-center">
                <div className="col-md-12">
                  <div className="page-header-title">
                    <h5 className="m-b-10">MiDulceOnline8 tienda virtual</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="card">
                <div className="card-header">
                  <h5>Ventas</h5>
                  <span className="d-block m-t-5"></span>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default home;
