import uniquid from 'uniqid';
import React, {useState} from 'react';
import {useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import APIInvoke from "../utils/APIInvoke";
import Menu from '../plantilla/menu';
import Logo from '../plantilla/logo';

function ProductosAgregar()
{
    const[nombre,setNombre] = useState('')
    const[unidades,setUnidades] = useState('')
    const[precio,setPrecio] = useState('')
    const[activo,setActivo] = useState('')
    const navegar = useNavigate()


    const productosInsertar = async () => {
        const data = {
        idProducto: uniquid(),
        nombreProducto: nombre,
        unidades: unidades,
        precio: precio,
        activo: activo
        }
    
        console.log(data)

    const responde = await APIInvoke.invokePOST(`/api/productos/save`, data);
    const mensaje = responde.msg; // mensaje que se coloca res.json() esta en el controller
    console.log(mensaje)
    if (mensaje !== "El producto fue guardado exitosamente") {
      const msg = "El producto  no fue creado";
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: msg,
      });

    } else {
      const msg = "El producto fue creado exitosamente..";
      Swal.fire({
        icon: 'success',
        title: 'Confirmación',
        text: msg,
      });
      navegar('/inicio')


    }
}
    
return(
    <div>
        <Menu/>
        <Logo/>
        <section className="pcoded-main-container">
            <div className='container mt-5'>
                <h4>Producto</h4>
                <div className='row'>
                    <div className='col-md-12'>
                        <div className="mb-3">
                            <label htmlFor="nombre" className="form-label">Nombre</label>
                            <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="unidades" className="form-label">Unidades</label>
                            <input type="text" className="form-control" id="unidades" value={unidades} onChange={(e)=>{setUnidades(e.target.value)}}></input>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="precio" className="form-label">Precio</label>
                            <input type="text" className="form-control" id="precio" value={precio} onChange={(e)=>{setPrecio(e.target.value)}}></input>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="activo" className="form-label">Activo</label>
                            <input type="text" className="form-control" id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                        </div>
                        <Link to={"/inicio"}> 
                        <button type="button" className="btn btn-info">Atras</button>
                        </Link>
                        <button type="button" className="btn btn-success" onClick={productosInsertar}>Agregar</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
)

}

export default ProductosAgregar;