import {Link} from 'react-router-dom';

function Register()
{

    return(

    <div className="auth-wrapper">
        <div className="auth-content text-center">
            <img src="assets/images/logo.png" alt="" className="img-fluid mb-4"></img>
            <div className="card borderless">
                <div className="row align-items-center text-center">
                    <div className="col-md-12">
                        <div className="card-body">
                            <h4 className="f-w-400">Registro</h4>
                            <hr></hr>
                            <div className="form-group mb-3">
                                <input type="text" className="form-control" id="nombre" placeholder="Nombre"></input>
                            </div>
                            <div className="form-group mb-3">
                                <input type="email" className="form-control" id="email" placeholder="Email"></input>
                            </div>
                            <div className="form-group mb-4">
                                <input type="password" className="form-control" id="password" placeholder="Password"></input>
                            </div>
                            <button className="btn btn-primary btn-block mb-4">Registrarse</button>
                            <hr></hr>
                            <p className="mb-2">Ya tienes una cuenta? <Link to={"/"} className="f-w-400">Inciar sesión</Link></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    )

}

export default Register
