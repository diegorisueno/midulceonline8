import React, {useState, useEffect } from 'react';
import {useNavigate, Link, useParams} from 'react-router-dom';
import Swal from 'sweetalert2';
import APIInvoke from "../utils/APIInvoke";
import Menu from '../plantilla/menu';
import Logo from '../plantilla/logo';

function ProductosEditar(){

    const navegar = useNavigate()
    const {idProducto} = useParams();

    const id = idProducto;

    const [dataProductos, setdataProductos] = useState([]);
    const consultarProducto = async () => {
    const response = await APIInvoke.invokeGET(`/api/productos/cargar/${id}`);
    setdataProductos(response.productos);  
    };

    useEffect(() => {
        document.getElementById("nombre").focus();
        consultarProducto();
        }, []);


    const productos = dataProductos[0];
    const setNombre = productos?.nombreProducto;
    const setUnidades = productos?.unidades;
    const setPrecio = productos?.precio;
    const setActivo = productos?.activo;

    console.log(dataProductos)

    const [producto, setProducto] = useState({});

    const {
        nombre = setNombre,
        unidades = setUnidades,
        precio = setPrecio,
        activo = setActivo,
        } = producto

    const onChange = (e) => {
        setProducto({
          ...producto,
          [e.target.name]: e.target.value,
        });
      };
    

    const productosUpdate = async () => {
        const data = {
        nombreProducto: producto.nombre,
        unidades: producto.unidades,
        precio: producto.precio,
        activo: producto.activo
        }
        console.log(data)

    const responde = await APIInvoke.invokePUT(`/api/productos/update/${id}`, data);
    const mensaje = responde.msg; // mensaje que se coloca res.json() esta en el controller
    console.log(mensaje)
    if (mensaje !== "actualizar el registro") {
      const msg = "El producto  no fue creado";
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: msg,
      });

    } else {
      const msg = "El producto fue creado exitosamente..";
      Swal.fire({
        icon: 'success',
        title: 'Confirmación',
        text: msg,
      });
      navegar('/inicio')
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        productosUpdate();
      };
return(
    <div>
    <Menu/>
    <Logo/>

        <section className="pcoded-main-container">
                <div className='container mt-5'>
                    <h4>Editar Producto</h4>
                    <div className='row'>
                        <div className='col-md-12'>
                            <form onSubmit={onSubmit}>
                                <div className="mb-3">
                                    <label htmlFor="nombre" className="form-label">Nombre</label>
                                    <input type="text" className="form-control"
                                    id="nombre" name="nombre" value={nombre} onChange={{onChange}}></input>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="unidades" className="form-label">Unidades</label>
                                    <input type="text" className="form-control"
                                    id="unidades" name="unidades" value={unidades} onChange={onChange}></input>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="precio" className="form-label">Precio</label>
                                    <input type="text" className="form-control"
                                    id="precio" name="precio" value={precio} onChange={onChange}></input>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="activo" className="form-label">Activo</label>
                                    <input type="text" className="form-control"
                                    id="activo" name="activo" value={activo} onChange={onChange}></input>
                                </div>
                                <Link to={"/inicio"}> 
                                <button type="button" className="btn btn-info">Atras</button>
                                </Link>
                                <button type="submit" className="btn btn-primary">Editar</button>
                            </form>
                        </div>
                    </div>
                </div>
        </section>
    </div>
)

}

export default ProductosEditar;