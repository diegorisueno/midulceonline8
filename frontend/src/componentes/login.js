import {Link} from 'react-router-dom';

function Login()
{

    return(
    <div className="auth-wrapper">
        <div className="auth-content text-center">

            <div className="card borderless">
                <div className="row align-items-center ">
                    <div className="col-md-12">
                        <div className="card-body">
                            <h4 className="mb-3 f-w-400">Iniciar sesión</h4>
                            <hr></hr>
                            <div className="form-group mb-3">
                                <input type="text" className="form-control" id="Email" placeholder="Email address"></input>
                            </div>
                            <div className="form-group mb-4">
                                <input type="password" className="form-control" id="Password" placeholder="Password"></input>
                            </div>
                            <div className="custom-control custom-checkbox text-left mb-4 mt-2">
                                <input type="checkbox" className="custom-control-input" id="customCheck1"></input>
                                <label className="custom-control-label" htmlFor="customCheck1">Recordarme</label>
                            </div>
                            <Link to={"/inicio"} className="btn btn-block btn-primary mb-4">Iniciar sesión</Link>
                            <hr></hr>
                            <p className="mb-2 text-muted">Olvidaste el password?<Link to={"/reset"} className="f-w-400">Restablecer</Link></p>
                            <p className="mb-0 text-muted">No tienes una cuenta<Link to={"/register"} className="f-w-400">Registrate</Link></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

}

export default Login
